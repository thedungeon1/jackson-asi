# Use a base image with a Linux distribution, such as Alpine Linux
FROM alpine:3.18

# Install kubectl
RUN apk --no-cache add curl
RUN curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl" && \
    chmod +x kubectl && \
    mv kubectl /usr/local/bin/

# Set a label to identify this image
LABEL maintainer="KubectlImage"

# Verify the kubectl installation
RUN kubectl version --client

# Set the default command to run kubectl
CMD ["kubectl"]
