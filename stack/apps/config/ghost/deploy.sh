#!/bin/bash

# Set kapp CLI options
KAPP_OPTIONS="--diff-changes=true"

# Deploy the application with kapp
kapp deploy -a jackson-asi -f appcr.yaml --namespace app-controller $KAPP_OPTIONS
